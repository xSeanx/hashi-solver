#include <stdlib.h>
#include "disjointed_set.h"

void disjoint_set::init(int amount, vector<int> degs) {
    #ifdef DEBUG_INIT
        printf("DJS initialize\n");
    #endif // DEBUG
	int i;
	hd.resize(amount + 1);
	sz.resize(amount + 1, 1);
	deg.resize(amount + 1);
	for (i = 1; i <= amount; ++i) hd[i] = i, deg[i] = degs[i];
    #ifdef DEBUG_INIT
        printf("DJS initialize finished\n");
    #endif // DEBUG
}
int disjoint_set::find(int p) {
	if (hd[p] == p) return p;
	return hd[p] = find(hd[p]);
}
bool disjoint_set::join(int p, int q) {
	p = find(p), q = find(q);
	if (p == q) return 0;
	if (sz[p] < sz[q]) hd[p] = q, sz[q] += sz[p], deg[q] += deg[p];
	else hd[q] = p, sz[p] += sz[q], deg[p] += deg[q];
	return 1;
}
vector<int>::iterator disjoint_set::get(int p) {
	return deg.begin() + find(p);
}
int disjoint_set::get_size(int p) {
	return sz[find(p)];
}
