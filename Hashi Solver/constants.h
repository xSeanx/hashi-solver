#ifndef _HASHI_CONSTANTS_
#define _HASHI_CONSTANTS_
#define _HASHI_DEBUG_DFS_
//#define _HASHI_DEBUG_INIT_
//#define _HASHI_DEBUG_CONNECT_
//#define _HASHI_DEBUG_PIGEONHOLE_

namespace constants {
    const int OPPOSITE_DIRECTION[4] = {2, 3, 0, 1};
    const int OPSD[4] = {2, 3, 0, 1};
    const int LEFT = 0;
    const int UP = 1;
    const int RIGHT = 2;
    const int DOWN = 3;
};

#endif // _HASHI_CONSTANTS_
