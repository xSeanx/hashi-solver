#include <stdio.h>
#include <stdlib.h>
#include "user-input.h"
#include "constants.h"

raw_data input(void) {
    raw_data res;
    int i, j;
    scanf("%d%d%d" , &res.n , &res.m , &res.node);
    res.map = (int**)malloc(sizeof(int*) * (res.n + 1));
    for (i = 1; i <= res.n; ++i) res.map[i] = (int*)malloc(sizeof(int) * (res.m + 1));
    for (i = 1; i <= res.n ; ++i){
        for(j = 1 ; j <= res.m ; ++j){
            scanf("%d" , &res.map[i][j]);
        }
    }
    return res;
}
