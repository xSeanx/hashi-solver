#include <bits/stdc++.h>
#include "graph.h"
#include "constants.h"
#include "selector.h"
using namespace std;

int (*selector)(graph*) = max_value;

long long counter = 1, edg = 0;
graph answer;
bool main_dfs(graph G) {
    ++counter;
    #ifdef _HASHI_DEBUG_DFS_
    if (!(counter % 10000)) {
        printf(".");
    }
    #endif // _HASHI_DEBUG_DFS_
    int i, j, k, a, b, c, d, w, x, y, z;
    vertex *v, *ot;
    #ifdef _HASHI_DEBUG_DFS_
    //printf("----\n");
    //G.connect_map.output();
    #endif // _HASHI_DEBUG_DFS_
    if (!G.pigeonhole_process()) {
        G.unfinished.clear();
        G.unfinished.shrink_to_fit();
        return 0;
    }
    #ifdef _HASHI_DEBUG_DFS_
    //G.connect_map.output();
    //printf("----\n");
    #endif // _HASHI_DEBUG_DFS_
    k = selector(&G);
    for (i = 0, j = 0; i < G.unfinished.size(); ++i) {
        if (G.VS[G.unfinished[i]]->num > j)
            j = G.VS[G.unfinished[i]]->num;
    }
    if (j == 0) {
        answer = G.duplicate_self();
        G.unfinished.clear();
        G.unfinished.shrink_to_fit();
        return 1;
    }
    a = G.VS[k]->edge[0];
    b = G.VS[k]->edge[1];
    c = G.VS[k]->edge[2];
    d = G.VS[k]->edge[3];
    #ifdef _HASHI_DEBUG_DFS_
    //printf("id:%d --> %d %d %d %d\n", G.VS[k]->id, a, b, c, d);
    #endif // _HASHI_DEBUG_DFS_
    for (w = 0; w <= a; ++w) {
        for (x = 0; x <= b; ++x) {
            for (y = 0; y <= c; ++y) {
                for (z = 0; z <= d; ++z) {
                    if (w + x + y + z == G.VS[k]->num) {
                        edg += x + y + w + z;
                        graph Gp = G.duplicate_self();
                        if (Gp.VS[k]->adj[0] != nullptr && w) Gp.connect(Gp.VS[k], Gp.VS[k]->adj[0], w, 0);
                        if (Gp.VS[k]->adj[1] != nullptr && x) Gp.connect(Gp.VS[k], Gp.VS[k]->adj[1], x, 1);
                        if (Gp.VS[k]->adj[2] != nullptr && y) Gp.connect(Gp.VS[k], Gp.VS[k]->adj[2], y, 2);
                        if (Gp.VS[k]->adj[3] != nullptr && z) Gp.connect(Gp.VS[k], Gp.VS[k]->adj[3], z, 3);
                        #ifdef _HASHI_DEBUG_DFS_
                        //printf("id:%d, %d %d %d %d\n", G.VS[k]->id, w, x, y, z);
                        #endif // _HASHI_DEBUG_DFS_
                        bool res = main_dfs(Gp);
                        #ifdef _HASHI_DEBUG_DFS_
                        //printf("returned here: %d\n", (int)res);
                        #endif // _HASHI_DEBUG_DFS_
                        if (res) {
                            G.unfinished.clear();
                            G.unfinished.shrink_to_fit();
                            return 1;
                        }
                        edg -= x + y + w + z;
                        Gp.destruct();
                    }
                }
            }
        }
    }
    G.unfinished.clear();
    G.unfinished.shrink_to_fit();
    return 0;
}

int main() {
    graph G;
    G.init(input());
    main_dfs(G);
    answer.connect_map.debug(1);
    printf("total edge enumed: %lld", edg);
}
