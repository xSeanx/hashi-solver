class disjoint_set {
	public:
		int node, groups, *head, *size, *deg;
		void init(int amount, int *degs);
        void destruct(void);
		int find(int p);
		bool join(int p, int q);
		int* get_deg(int p);
		int get_size(int p);
        void duplicate_from(disjoint_set *ot);
        void debug(void);
};

