#ifndef _HASHI_USER_INPUT_
#define _HASHI_USER_INPUT_

struct raw_data {
    int n, m, node, **map;
};
raw_data input(void);

#endif // _HASHI_USER_INPUT_
