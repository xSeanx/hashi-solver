#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <set>
#include "graph.h"
#include "constants.h"
using namespace constants;
using namespace std;

void graph::destruct() {
    int i;
    for (i = 1; i <= node; ++i) {
        VS[i]->destruct();
        free(VS[i]);
    }
    for (i = 1; i <= n; ++i) {
        free(vert_map[i]);
        free(map[i]);
    }
    free(VS);
    free(vert_map);
    free(map);
    adjmat.destruct();
    connect_map.destruct();
    dsu.destruct();
    return;
}
void graph::init(raw_data data) {
    n = data.n, m = data.m, node = data.node;
    map = data.map;
    #ifdef _HASHI_DEBUG_INIT_
    printf("Graph initialize: memory allocation\n");
    #endif // _HASHI_DEBUG_INIT_
    int i, j, x, y, v;
    bool flag;
    vertex *ot;
    VS = (vertex**)malloc(sizeof(vertex*) * (node + 1));
    for (i = 1; i <= node; ++i) {
        VS[i] = (vertex*)malloc(sizeof(vertex));
        VS[i]->adj = (vertex**)malloc(sizeof(vertex*) * 4);
        for (j = 0; j < 4; ++j) {
            VS[i]->way[j] = 0;
            VS[i]->edge[j] = 0;
            VS[i]->adj[j] = nullptr;
        }
    }
    vert_map = (vertex***)malloc(sizeof(vertex**) * (n + 1));
    for (i = 1; i <= n; ++i) vert_map[i] = (vertex**)malloc(sizeof(vertex*) * (m + 1));
    for (i = 1, v = 1; i <= n; ++i) {
        for (j = 1; j <= m; ++j) {
            if (map[i][j]) {
                VS[v]->x = i, VS[v]->y = j;
                VS[v]->id = v, VS[v]->num = VS[v]->orig_num = map[i][j];
                vert_map[i][j] = VS[v];
                v++;
            }
        }
    }
    for (i = 1; i <= node; ++i) unfinished.push_back(i);
    #ifdef _HASHI_DEBUG_INIT_
    printf("Graph initialize: recognize neighbor\n");
    printf("  L   U   R   D\n");
    #endif // _HASHI_DEBUG_INIT_
    for (i = 1; i <= node; ++i) {
        x = VS[i]->x, y = VS[i]->y;
        #ifdef _HASHI_DEBUG_INIT_
        printf("%3d ", i);
        #endif // _HASHI_DEBUG_INIT_
        for (j = y - 1, flag = 0; j > 0; --j) {
            if (map[x][j]) {
                flag = 1;
                break;
            }
        }
        if (flag) {
            ot = vert_map[x][j];
            VS[i]->way[0] = 1;
            VS[i]->edge[0] = min(2, min(ot->num, VS[i]->num));
            VS[i]->adj[0] = ot;
        }
        #ifdef _HASHI_DEBUG_INIT_
        printf("%3d ", i);
        #endif // _HASHI_DEBUG_INIT_
        for (j = x - 1, flag = 0; j > 0; --j) {
            if (map[j][y]) {
                flag = 1;
                break;
            }
        }
        if (flag) {
            ot = vert_map[j][y];
            VS[i]->way[1] = 1;
            VS[i]->edge[1] = min(2, min(ot->num, VS[i]->num));
            VS[i]->adj[1] = ot;
        }
        #ifdef _HASHI_DEBUG_INIT_
        printf("%3d\n", i);
        #endif // _HASHI_DEBUG_INIT_
        for (j = y + 1, flag = 0; j <= m; ++j) {
            if (map[x][j]) {
                flag = 1;
                break;
            }
        }
        if (flag) {
            ot = vert_map[x][j];
            VS[i]->way[2] = 1;
            VS[i]->edge[2] = min(2, min(ot->num, VS[i]->num));
            VS[i]->adj[2] = ot;
        }
        #ifdef _HASHI_DEBUG_INIT_
        printf("%3d ", i);
        #endif // _HASHI_DEBUG_INIT_
        for (j = x + 1, flag = 0; j <= n; ++j) {
            if (map[j][y]) {
                flag = 1;
                break;
            }
        }
        if (flag) {
            ot = vert_map[j][y];
            VS[i]->way[3] = 1;
            VS[i]->edge[3] = min(2, min(ot->num, VS[i]->num));
            VS[i]->adj[3] = ot;
        }
    }
    #ifdef _HASHI_DEBUG_INIT_
    printf("Graph initialize: child object initialize\n");
    #endif // _HASHI_DEBUG_INIT_
    adjmat.init(node);
    int *deg = (int*)malloc(sizeof(int) * (node) + 1);
    for (i = 1; i <= node; ++i) deg[i] = VS[i]->num;
    dsu.init(node, deg);
    connect_map.init(n, m);
    connect_map.set_number_map(map);
    #ifdef _HASHI_DEBUG_INIT_
    printf("Graph initialize finished\n");
    printf("vertex information chart\nid\tx\ty\tnum\tleft\tup\tright\tdown\n");
    for (i = 1; i <= node; ++i) {
        printf("%d\t", VS[i]->id);
        printf("%d\t", VS[i]->x);
        printf("%d\t", VS[i]->y);
        printf("%d\t", VS[i]->num);
        printf("%d\t", (VS[i]->adj[0] == nullptr)?-1:VS[i]->adj[0]->num);
        printf("%d\t", (VS[i]->adj[1] == nullptr)?-1:VS[i]->adj[1]->num);
        printf("%d\t", (VS[i]->adj[2] == nullptr)?-1:VS[i]->adj[2]->num);
        printf("%d\n", (VS[i]->adj[3] == nullptr)?-1:VS[i]->adj[3]->num);
    }
    #endif // _HASHI_DEBUG_INIT_
}
void graph::connect(vertex *v, vertex *ot, int amount, int dir) {
    #ifdef _HASHI_DEBUG_CONNECT_
    printf("connect %d edges between vertex %d and vertex %d", amount, v->id, ot->id);
    #endif // _HASHI_DEBUG_CONNECT_
    int opdir = OPSD[dir];
	ot->num -= amount, v->num -= amount;
	v->edge[dir] -= amount, ot->edge[opdir] -= amount;
	connect_map.modify(v->x, v->y, ot->x, ot->y, amount, dir);
	connect_map.map[v->x][v->y] -= amount;
	connect_map.map[ot->x][ot->y] -= amount;
	dsu.join(v->id, ot->id);
	*dsu.get_deg(v->id) -= amount * 2;
	adjmat.add(v->id, ot->id, amount);
	if (ot->num == 0 || v->num == 0) v->way[dir] = 0, ot->way[opdir] = 0, v->edge[dir] = 0, ot->edge[opdir] = 0;
    modified = true;
    #ifdef _HASHI_DEBUG_CONNECT_
    printf(", successfully connect\n");
    #endif // _HASHI_DEBUG_CONNECT_
}
bool graph::pigeonhole(vertex *v) {
	int i, w;
	vertex *ot;
    #ifdef _HASHI_DEBUG_PIGEONHOLE_
    printf("pigeonhole: update vertex information");
    #endif // _HASHI_DEBUG_PIGEONHOLE_
    for (i = 0; i < 4; ++i) {
        if (!v->way[i]) continue;
        ot = v->adj[i];
        w = min(v->edge[i], min(2, min(v->num, ot->num)));
        if (w) {
            v->way[i] = ot->way[OPSD[i]] = 1;
            v->edge[i] = ot->edge[OPSD[i]] = w;
        } else {
            v->way[i] = ot->way[OPSD[i]] = 0;
            v->edge[i] = ot->edge[OPSD[i]] = 0;
        }
    }
	for (i = 0; i < 4; ++i) {
        if (v->way[i]) {
            ot = v->adj[i];
            if (!adjmat.get(v->id, ot->id) && connect_map.query(v->x, v->y, ot->x, ot->y, i)) {
                v->edge[i] = ot->edge[OPSD[i]] = 0;
                v->way[i] = ot->way[OPSD[i]] = 0;
            }
        }
	}
	if (*dsu.get_deg(v->id) == 1 && dsu.groups > 2) {
		for (i = 0; i < 4; ++i) {
            if (!v->way[i]) continue;
			ot = v->adj[i];
			if (*dsu.get_deg(ot->id) == 1) {
				v->edge[i] = ot->edge[OPSD[i]] = 0;
                v->way[i] = ot->way[OPSD[i]] = 0;
			}
		}
	}
	#ifdef _HASHI_DEBUG_PIGEONHOLE_
    printf("pigeonhole vertex infromation:\n");
    v->debug();
	#endif // _HASHI_DEBUG_PIGEONHOLE_
    if (v->num == 0) return !(dsu.groups > 2 && *dsu.get_deg(v->id) == 0);
    if (v->total_edge() < v->num) return 0;
    //
	if (v->num == v->total_edge()) {
		for (i = 0; i < 4; ++i) {
            if (!v->way[i]) continue;
			connect(v, v->adj[i], v->edge[i], i);
		}
	}
	//
	if ((v->num + 1) / 2 == v->total_way()) {
		for (i = 0; i < 4; ++i) {
            if (!v->way[i]) continue;
			connect(v, v->adj[i], 1, i);
		}
	}
	//
	for (i = w = 0; i < 4; ++i) {
		if (!v->way[i]) continue;
		if (v->adj[i]->num == 1) {
			w++;
		}
	}
	if ((v->num - w + 1) / 2 == v->total_way() - w) {
		for (i = 0; i < 4; ++i) {
            if (!v->way[i]) continue;
			if (v->adj[i]->num != 1) {
				connect(v, v->adj[i], 1, i);
			}
		}
	}
	if (v->orig_num == 2 && v->num == 2 && v->total_way() == 2) {
        for (i = 0, w = 0; i < 4; ++i) {
            if (v->way[i] && v->adj[i]->orig_num == 2 && v->adj[i]->num == 2) {
                ++w;
            }
        }
        if (w == 1) {
            for (i = 0; i < 4; ++i) {
                if (v->way[i] && v->adj[i]->orig_num != 2) {
                    connect(v, v->adj[i], 1, i);
                }
            }
        } else if (w == 2) {
            for (i = 0; i < 4; ++i) {
                if (v->way[i]) {
                    connect(v, v->adj[i], 1, i);
                }
            }
        }
	}
	return 1;
}
graph graph::duplicate_self(void) {
    graph res;
    res.n = n, res.m = m, res.node = node, res.total_dots = total_dots;
    // pass
    int i, j;
    res.vert_map = (vertex***)malloc(sizeof(vertex**) * (n + 1));
    res.map = (int**)malloc(sizeof(int*) * (n + 1));
    for (i = 1; i <= n; ++i) {
        res.vert_map[i] = (vertex**)malloc(sizeof(vertex*) * (m + 1));
        res.map[i] = (int*)malloc(sizeof(int) * (m + 1));
    }
    res.dsu.duplicate_from(&dsu);
    res.connect_map.duplicate_from(&connect_map);
    res.adjmat.duplicate_from(&adjmat);
    res.VS = (vertex**)malloc(sizeof(vertex*) * (node + 1));
    for (i = 1; i <= node; ++i) {
        res.VS[i] = (vertex*)malloc(sizeof(vertex));
        res.VS[i]->duplicate_from(VS[i]);
        res.vert_map[res.VS[i]->x][res.VS[i]->y] = res.VS[i];
    }
    for (i = 1; i <= node; ++i) {
        for (j = 0; j < 4; ++j) {
            if (VS[i]->adj[j] == nullptr) res.VS[i]->adj[j] = nullptr;
            else res.VS[i]->adj[j] = res.VS[VS[i]->adj[j]->id];
        }
    }
    for (i = 1; i <= n; ++i) {
        for (j = 1; j <= m; ++j) {
            res.map[i][j] = map[i][j];
        }
    }
    for (i = 0; i < unfinished.size(); ++i) {
        res.unfinished.push_back(unfinished[i]);
    }
    return res;
}
bool graph::pigeonhole_process() {
    set<int> st(unfinished.begin(), unfinished.end());
    set<int>::iterator itr;
    int i, check[node + 1], t = 0;
    while (1) {
        modified = false;
        for (int el: st) {
            if (!pigeonhole(VS[el])) {
                st.clear();
                return 0;
            }
        }
        fill(check, check + node + 1, 0);
        for (i = 1; i <= node; ++i) {
            if (VS[i]->num == 0) {
                check[i] = 1;
            }
        }
        for (i = 1; i <= node; ++i) {
            if (check[i] && (itr = st.find(i)) != st.end()) {
                st.erase(itr);
            }
        }
        if (t == 0 && !modified) {
            st.clear();
            return 0;
        }
        if (!modified) break;
        t++;
    }
    unfinished.clear();
    for (itr = st.begin(); itr != st.end(); ++itr) {
        unfinished.push_back(*itr);
    }
    for (i = 1; i <= node; ++i) {
        if (VS[i]->num) {
            st.clear();
            return 1;
        }
    }
    st.clear();
    return dsu.get_size(1) == node;
}
