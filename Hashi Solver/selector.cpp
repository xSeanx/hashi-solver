#include "selector.h"
#include <stdio.h>
#include <climits>

int max_value(graph *G) {
    int i, mx, res;
    for (i = 0, mx = INT_MIN; i < G->unfinished.size(); ++i) {
        if (G->VS[G->unfinished[i]]->num > mx)
            res = G->unfinished[i], mx = G->VS[G->unfinished[i]]->num;
    }
    return res;
}
int min_value(graph *G) {
    int i, mi, res;
    for (i = 0, mi = INT_MAX; i < G->unfinished.size(); ++i) {
        if (G->VS[G->unfinished[i]]->num < mi)
            res = G->unfinished[i], mi = G->VS[G->unfinished[i]]->num;
    }
    return res;
}
int max_deg(graph *G) {
    int i, mx, res;
    for (i = 0, mx = INT_MIN; i < G->unfinished.size(); ++i) {
        if (*(G->dsu.get_deg(G->unfinished[i])) > mx)
            res = G->unfinished[i], mx = *(G->dsu.get_deg(G->unfinished[i]));
    }
    return res;
}
int min_deg(graph *G) {
    int i, mi, res;
    for (i = 0, mi = INT_MAX; i < G->unfinished.size(); ++i) {
        if (*(G->dsu.get_deg(G->unfinished[i])) < mi)
            res = G->unfinished[i], mi = *(G->dsu.get_deg(G->unfinished[i]));
    }
    return res;
}
int max_size(graph *G) {
    int i, mx, res;
    for (i = 0, mx = INT_MIN; i < G->unfinished.size(); ++i) {
        if (G->dsu.get_size(G->unfinished[i]) > mx)
            res = G->unfinished[i], mx = G->dsu.get_size(G->unfinished[i]);
    }
    return res;
}
int min_size(graph *G) {
    int i, mi, res;
    for (i = 0, mi = INT_MAX; i < G->unfinished.size(); ++i) {
        if (G->dsu.get_size(G->unfinished[i]) < mi)
            res = G->unfinished[i], mi = G->dsu.get_size(G->unfinished[i]);
    }
    return res;
}
