#ifndef _PLANE_OPERATOR_H_
#define _PLANE_OPERATOR_H_
#include <vector>
using namespace std;

class plane {
    public:
        void init(int w, int h);
        int query(int x1, int y1, int x2, int y2);
        void modify(int x1, int x2, int y1, int y2, int val);
        void output();
        void set_vertex_map(vector<vector<int>> vertex_map);
    private:
        vector<vector<int>> mp, v_mp;
        int n, m;
};

#endif // _PLANE_OPERATOR_H_
