#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include "plane.h"
#include "constants.h"
using namespace std;

void plane::init(int width, int height) {
    #ifdef _HASHI_DEBUG_INIT_
    printf("plane initialize started\n");
    #endif // _HASHI_HASHI_DEBUG_INIT_
    int i;
    n = width, m = height;
    map = (int**)malloc(sizeof(int*) * (n + 1));
    direction = (int**)malloc(sizeof(int*) * (n + 1));
    for (i = 1; i <= n; ++i) {
        map[i] = (int*)calloc(m + 1, sizeof(int));
        direction[i] = (int*)calloc(m + 1, sizeof(int));
    }
    #ifdef _HASHI_DEBUG_INIT_
    printf("plane initialize finished\n");
    #endif // _HASHI_DEBUG_INIT_
}
void plane::destruct(void) {
    int i;
    for (i = 1; i <= n; ++i) {
        free(map[i]);
        free(direction[i]);
    }
    free(map);
    free(direction);
}
void plane::duplicate_from(plane *ot) {
    int i, j;
    m = ot->m, n = ot->n;
    num_map = ot->num_map;
    map = (int**)malloc(sizeof(int*) * (n + 1));
    direction = (int**)malloc(sizeof(int*) * (n + 1));
    for (i = 1; i <= n; ++i) {
        direction[i] = (int*)malloc(sizeof(int) * (m + 1));
        map[i] = (int*)malloc(sizeof(int) * (m + 1));
    }
    for (i = 1; i <= n; ++i) {
        for (j = 1; j <= m; ++j) {
            map[i][j] = ot->map[i][j];
            direction[i][j] = ot->direction[i][j];
        }
    }
}
bool plane::query(int x1, int y1, int x2, int y2, int dir) {
    int i;
    if (dir == 0 || dir == 2) {
        if (y1 > y2) swap(y1, y2);
        for (i = y1 + 1; i < y2; ++i) {
            if (map[x1][i]) return 1;
        }
    } else if (dir == 1 || dir == 3) {
        if (x1 > x2) swap(x1, x2);
        for (i = x1 + 1; i < x2; ++i) {
            if (map[i][y1]) return 1;
        }
    } else {
        printf("Unknown direction has just passed into the plane modify function\n");
    }
    return 0;
}
void plane::modify(int x1, int y1, int x2, int y2, int value, int dir) {
    int i;
    if (dir == 0 || dir == 2) {
        if (y1 > y2) swap(y1, y2);
        for (i = y1 + 1; i < y2; ++i) {
            map[x1][i] += value;
            direction[x1][i] = 1;
        }
    } else if (dir == 1 || dir == 3) {
        if (x1 > x2) swap(x1, x2);
        for (i = x1 + 1; i < x2; ++i) {
            map[i][y1] += value;
            direction[i][y1] = 2;
        }
    } else {
        printf("Unknown direction has just passed into the plane modify function\n");
    }
}
void plane::debug(int mode = 1) {
    int i, j;
    printf("(debug) Plane:\n%d %d\n", n, m);
    for (i = 1; i <= n; ++i) {
        for (j = 1; j <= m; ++j) {
            if (num_map[i][j]) {
                printf(" %d ", (mode == 1)? num_map[i][j]: map[i][j]);
            } else if (map[i][j] == 2){
                if (direction[i][j] == 1) {
                    printf(" = ");
                } else {
                    printf("|| ");
                }
            } else if (map[i][j] == 1) {
                if (direction[i][j] == 1) {
                    printf(" - ");
                } else {
                    printf(" | ");
                }
            } else {
                printf("   ");
            }
        }
        putchar('\n');
    }
}
void plane::set_number_map(int **number_map) {
    int i, j;
    num_map = number_map;
    for (i = 1; i <= n; ++i) {
        for (j = 1; j <= m; ++j) {
            map[i][j] = number_map[i][j];
        }
    }
}
