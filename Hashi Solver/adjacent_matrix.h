#ifndef _ADJCENT_MATRIX_H_
#define _ADJCENT_MATRIX_H_
#include <vector>
using namespace std;

class adjacent_matrix {
    public:
        vector<vector<int>> mat;
        void init(int node);
        int get(int x, int y);
        int add(int x, int y);
};

#endif // _ADJCENT_MATRIX_H_
