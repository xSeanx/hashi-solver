#ifndef _HASHI_GRAPH_
#define _HASHI_GRAPH_
#include <vector>
#include "vertex.h"
#include "plane.h"
#include "adjacent-matrix.h"
#include "disjoint-set.h"
#include "user-input.h"
using namespace std;

class graph {
    public:
        vector<int> unfinished;
        bool modified;
        int n, m, node, total_dots, **map;
        vertex ***vert_map;
        vertex **VS;
        disjoint_set dsu;
        plane connect_map;
        adjacent_matrix adjmat;
        void init(raw_data data);
        void destruct(void);
        graph duplicate_self(void);
        void connect(vertex *v, vertex *ot, int amount, int dir);
        bool pigeonhole(vertex *v);
        bool pigeonhole_process(void);
};

#endif // _HASHI_GRAPH_
