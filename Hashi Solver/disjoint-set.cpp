#include <stdlib.h>
#include <stdio.h>
#include "disjoint-set.h"
#include "constants.h"

void disjoint_set::destruct(void) {
    free(head);
    free(size);
    free(deg);
}
void disjoint_set::duplicate_from(disjoint_set *ot) {
    int i;
    node = ot->node, groups = ot->groups;
	head = (int*)malloc(sizeof(int) * (node + 1));
	size = (int*)malloc(sizeof(int) * (node + 1));
	deg = (int*)malloc(sizeof(int) * (node + 1));
	for (i = 1; i <= node; ++i) head[i] = ot->head[i], size[i] = ot->size[i], deg[i] = ot->deg[i];
}
void disjoint_set::init(int node, int *deg) {
    #ifdef _DEBUG_INIT_
    printf("DJS initialize started\n");
    #endif // DEBUG
	int i;
    this->node = groups = node;
	head = (int*)malloc(sizeof(int) * (node + 1));
	size = (int*)malloc(sizeof(int) * (node + 1));
	this->deg = (int*)malloc(sizeof(int) * (node + 1));
	for (i = 1; i <= node; ++i) head[i] = i, size[i] = 1, this->deg[i] = deg[i];
    #ifdef _DEBUG_INIT_
    printf("DJS initialize finished\n");
    #endif // DEBUG
}
int disjoint_set::find(int p) {
	if (head[p] == p) return p;
	return head[p] = find(head[p]);
}
bool disjoint_set::join(int p, int q) {
	p = find(p), q = find(q);
	if (p == q) return 0;
	if (size[p] < size[q]) head[p] = q, size[q] += size[p], deg[q] += deg[p];
	else head[q] = p, size[p] += size[q], deg[p] += deg[q];
	--groups;
	return 1;
}
int* disjoint_set::get_deg(int p) {
	return deg + find(p);
}
int disjoint_set::get_size(int p) {
	return size[find(p)];
}
void disjoint_set::debug(void) {
    int i;
    for (i = 1; i <= node; ++i) head[i] = find(i);
    printf("(debug) Disjoint Set:");
    printf("\nvertex ");
    for (i = 1; i <= node; ++i) printf("%3d ", i);
    printf("\nhead   ");
    for (i = 1; i <= node; ++i) printf("%3d ", head[i]);
    printf("\nsize   ");
    for (i = 1; i <= node; ++i) printf("%3d ", size[head[i]]);
    printf("\ndegree ");
    for (i = 1; i <= node; ++i) printf("%3d ", deg[head[i]]);
}
