#ifndef _HASHI_SELECTOR_
#define _HASHI_SELECTOR_
#include "graph.h"

int max_value(graph *G);
int min_value(graph *G);
int max_deg(graph *G);
int min_deg(graph *G);
int max_size(graph *G);
int min_size(graph *G);

#endif // _HASHI_SELECTOR_
