#ifndef _DISJOINTED_SET_H_
#define _DISJOINTED_SET_H_
#include <vector>
using namespace std;

class disjoint_set {
	public:
		void init(int amount, vector<int> degs);
		int find(int p);
		bool join(int p, int q);
		vector<int>::iterator get(int p);
		int get_size(int p);
	private:
		vector<int> hd, sz, deg;
};

#endif // _DISJOINTED_SET_H_

