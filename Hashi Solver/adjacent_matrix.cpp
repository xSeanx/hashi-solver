#include <stdlib.h>
#include "adjacent_matrix.h"

void adjacent_matrix::init(int node) {
    mat.resize(node + 1, vector<int>(node + 1, 0));
}
int adjacent_matrix::add(int x, int y) {
    if (mat[x][y] == 2) return mat[x][y];
    return ++mat[x][y];
}
int adjacent_matrix::get(int x, int y) {
    return mat[x][y];
}
