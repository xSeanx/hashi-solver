#include <stdlib.h>
#include <stdio.h>
#include "constants.h"
#include "vertex.h"

void vertex::destruct() {
    free(adj);
}
int vertex::total_edge(void) {
	return edge[0] + edge[1] + edge[2] + edge[3];
}
int vertex::total_way(void) {
	return way[0] + way[1] + way[2] + way[3];
}
void vertex::debug(void) {
    printf("\
+--------------------------------+\n \
| vertex debug:                  |\n \
|   id: %3d                      |\n \
|   now number: %3d              |\n \
|   position (x, y): (%3d, %3d)  |\n \
| +--------+----+----+----+----+ |\n \
| | info   | L  | U  | R  | D  | |\n \
| +--------+----+----+----+----+ |\n \
| | number |%3d |%3d |%3d |%3d | |\n \
| +--------+----+----+----+----+ |\n \
| | way    |%3d |%3d |%3d |%3d | |\n \
| +--------+----+----+----+----+ |\n \
| | edge   |%3d |%3d |%3d |%3d | |\n \
| +--------+----+----+----+----+ |\n \
+--------------------------------+\n",
        id, num, x, y,
        (adj[0] == nullptr)? -1:adj[0]->num,
        (adj[1] == nullptr)? -1:adj[1]->num,
        (adj[2] == nullptr)? -1:adj[2]->num,
        (adj[3] == nullptr)? -1:adj[3]->num,
        way[0], way[1], way[2], way[3],
        edge[0], edge[1], edge[2], edge[3]
    );
}
void vertex::duplicate_from(vertex *ot) {
    int i;
    num = ot->num, orig_num = ot->orig_num, id = ot->id;
    x = ot->x, y = ot->y;
    for (i = 0; i < 4; ++i) edge[i] = ot->edge[i], way[i] = ot->way[i];
    adj = (vertex**)malloc(sizeof(vertex*) * 4);
}

