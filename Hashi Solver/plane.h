#ifndef _HASHI_PLANE_
#define _HASHI_PLANE_

class plane {
    public:
        int **map, n, m, **num_map, **direction;
        void init(int width, int height);
        void destruct(void);
        void duplicate_from(plane *ot);
        bool query(int x1, int y1, int x2, int y2, int dir);
        void modify(int x1, int x2, int y1, int y2, int value, int dir);
        void debug(int mode);
        void set_number_map(int **number_map);
};

#endif // _HASHI_PLANE_
