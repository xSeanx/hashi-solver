#include <stdlib.h>
#include <utility>
#include <stdio.h>
#include "plane_operator.h"
using namespace std;

void plane::init(int w, int h) {
    #ifdef DEBUG_INIT
        printf("plane initialize\n");
    #endif // DEBUG
    mp.resize(w + 1, vector<int>(h + 1, 0));
    n = w, m = h;
    #ifdef DEBUG_INIT
        printf("plane initialize finished\n");
    #endif // DEBUG
}
int plane::query(int x1, int y1, int x2, int y2) {
    if (x1 > x2) swap(x1, x2);
    if (y1 > y2) swap(y1, y2);
    int i, j;
    for (i = x1; i <= x2; ++i) {
        for (j = y1; j <= y2; ++j) {
            if (mp[i][j]) return 1;
        }
    }
    return 0;
}
void plane::modify(int x1, int y1, int x2, int y2, int val) {
    int i, j;
    for (i = x1; i <= x2; ++i) {
        for (j = y1; j <= y2; ++j) {
            mp[i][j] += val;
        }
    }
}
void plane::output() {
    int i, j;
    printf("%d %d\n", n, m);
    for (i = 1; i <= n; ++i) {
        for (j = 1; j <= m; ++j) {
            if (v_mp[i][j]) {
                printf("%d ", mp[i][j]);
            } else if (mp[i][j] == 2){
                printf("= ");
            } else if (mp[i][j] == 1) {
                printf("- ");
            } else {
                printf("  ");
            }
        }
        printf("\n");
    }
}
void plane::set_vertex_map(vector<vector<int>> vertex_map) {
    v_mp = vertex_map;
}
