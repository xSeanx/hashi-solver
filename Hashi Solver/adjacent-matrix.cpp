#include <stdlib.h>
#include <stdio.h>
#include "adjacent-matrix.h"
#include "constants.h"

void adjacent_matrix::destruct(void) {
    for (int i = 1; i <= node; ++i) free(mat[i]);
    free(mat);
}
void adjacent_matrix::init(int node) {
    #ifdef _DEBUG_INIT_
    printf("adjacent matrix initialize started\n");
    #endif // _DEBUG_INIT_
    this->node = node;
    mat = (int**)malloc(sizeof(int*) * (node + 1));
    for (int i = 1; i <= node; ++i) mat[i] = (int*)calloc(node + 1, sizeof(int));
    #ifdef _DEBUG_INIT_
    printf("adjacent matrix initialize finished\n");
    #endif // _DEBUG_INIT_
}
void adjacent_matrix::duplicate_from(adjacent_matrix *ot) {
    int i, j;
    node = ot->node;
    mat = (int**)malloc(sizeof(int*) * (node + 1));
    for (i = 1; i <= node; ++i) mat[i] = (int*)malloc(sizeof(int) * (node + 1));
    for (i = 1; i <= node; ++i) for (j = 1; j <= node; ++j) mat[i][j] = ot->mat[i][j];
}
int adjacent_matrix::get(int x, int y) {
    return mat[x][y];
}
void adjacent_matrix::add(int x, int y, int value) {
    mat[x][y] += value;
    mat[y][x] += value;
}
void adjacent_matrix::debug(void) {
    int i, j;
    printf("(debug) Adjacent Matrix:\nvertex ");
    for (i = 1; i <= node; ++i) printf("| %3d ", i);
    for (i = 1; i <= node; ++i) {
        printf("\n-------");
        for (j = 1; j <= node; ++j) printf("+-----");
        printf("\n   %3d ", i);
        for (j = 1; j <= node; ++j) {
            printf("| %3d ", mat[i][j]);
        }
    }
}
