#ifndef _HASHI_ADJ_MAT_
#define _HASHI_ADJ_MAT_

class adjacent_matrix {
    /*
    The purpose of adjacent matrix is to record the number of edges two
    vertices are connected with. Thus, the program can easily access
    this information.
    */
    public:
        /*
        variables
        1. int** mat: store the amount of edges connected
        2. int node: the number of vertices
        */
        int **mat, node;
        /*
        functions
        1. void init(int node)
            initiate the adjacent matrix with vertices number "node"
        2. void duplicate_from(adjacent_matrix *ot)
            deepcopy "ot" to overwrite this adjacent matrix
        3. void destruct(void)
            destruct the matrix
        4. int get(int x, int y)
            get the edge number of x and y
        5. void add(int x, int y, int value)
            add "value" to the edge number of x and y
        6. void debug(void)
            print debug information
        */
        void init(int node);
        void duplicate_from(adjacent_matrix *ot);
        void destruct(void);
        int get(int x, int y);
        void add(int x, int y, int value);
        void debug(void);
};

#endif // _HASHI_ADJ_MAT_
