#ifndef _HASHI_VERTEX_
#define _HASHI_VERTEX_

class vertex {
	public:
		int num, orig_num, id, x, y;
		int edge[4], way[4];
		vertex **adj;
		/*
		0 left
		1 up
		2 right
		3 down
		*/
		int total_edge(void);
		int total_way(void);
		void debug(void);
        void duplicate_from(vertex *ot);
        void destruct(void);
};

#endif // _HASHI_VERTEX_
